const {
    app,
    BrowserWindow,
    Menu,
    Tray
} = require('electron')

require('update-electron-app')();

import { CTApp, CTApi, CTSsb} from 'collabthings-api';
import * as CTDocs from 'collabthings-app-docs';

let mainWindow: any;
let tray: any = null;

let ctapi: CTApi;
let ctapp: CTApp = new CTApp();

ctapp.init().then(() => {
	ctapi = ctapp.getApi();
	var docs:CTDocs.CTDocsApp = new CTDocs.CTDocsApp();
	docs.register(ctapi);
	
	ctapi.start();
	
	console.log("CTApp created"); 
}).catch((err: string) => {
	if(err) {
		console.log("CTApp creation error " + err);
	}
});

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600
    })

    // and load the index.html of the app.
    mainWindow.loadFile('index.html')

    tray = new Tray('assets/imgs/logo.png')
    tray.setToolTip('Collabthings.')

    const contextMenu = Menu.buildFromTemplate([{
            label: 'Show/Hide',
            click: () => {
                if (mainWindow.isVisible()) {
                    mainWindow.hide()
                } else {
                    mainWindow.show()
                }
            }
        },
        {
            label: 'Quit',
            click: () => {
                console.log("quit")
                app.quit()
            }
        }
    ])

    tray.setContextMenu(contextMenu)

    console.log("tray created")

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()
}

app.on('ready', createWindow);


app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
});
