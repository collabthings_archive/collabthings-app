// Client
import * as Mustache from 'mustache';
import * as JQuery from 'jquery';

class Client {
    getVersions(data: ViewData) {
        console.log( "process " + JSON.stringify( process ) );

        var p: any = process;
        if ( ( typeof p.versions.electron ) == 'undefined' ) {
            // default versions
        } else {
            data.node_version = process.versions.node;
            data.electron_version = p.versions.electron;
            data.chrome_version = p.versions.chrome;
        }
        console.log( "data " + JSON.stringify( data ) );
    }
}

class ViewData {
    public node_version: string;
    public chrome_version: string;
    public electron_version: string;

    public applist: any[];
    public userlist: any;
    public userinfo: any;

    public api: string;

    public userlist_empty: boolean;
    public follow_jeukku: string;
}

var url = "http://localhost:14881";

var client = new Client();

type AppList = Array<any>;

var data: ViewData = new ViewData();

function update() {
    client.getVersions(data);
    console.log( JSON.stringify( "Data : " + data ) );

    var temp = $( "#template" ).html();
    $( "#content" ).html( Mustache.render( temp, data ) );
    console.log( "updated" );
}

function initApp() {
    $.ajax( url + "/apps" ).done(( c: string ) => {
        var apps: any = JSON.parse( c );
        console.log( "apps " + c );

        var capps: any[] = [];

        var count: number = 0;
        for ( var i in apps ) {
            var key = i;
            var app: any = apps[key];
            app.launch = url + "/" + app.name;
            console.log( "adding app " + JSON.stringify( app ) );
            capps[count++] = app;
            //capps += key + "=>" + JSON.stringify(app) + " ";
        }

        data.applist = capps;
        update();
    } );


    $.ajax( url + "/me" ).done(( c: string ) => {
        var userinfo: any = JSON.parse( c );
        console.log( "userinfo " + c );
        data.userinfo = userinfo;

        data.api = url;

        update();
    } );
}

function initContacts() {
    console.log( "initContacts" );

    $.ajax( url + "/users" ).done(( c: string ) => {
        var userlist: any[] = JSON.parse( c );
        if ( Object.keys( userlist ).length == 0 ) {
            console.log( "userlist empty" );

            data.userlist_empty = true;

            var followstr: string = data.api + "/users/follow/" + encodeURIComponent( "@6H9x/fq6Iq+NWs05WSzMToYuBQ02f2BMzwnhTtxr/ck=.ed25519" );
            if ( followstr !== data.follow_jeukku ) {
                console.log( "update jeukku url " + followstr );
                data.follow_jeukku = followstr;
                update();
            }

            setTimeout( initContacts, 3000 );
        } else {
            console.log( "userlist not empty " + c );
            var list: any[] = [];

            for ( var uid in userlist ) {
                console.log( "uid:" + uid );
                var u: {} = userlist[uid];
                console.log( "user in list " + JSON.stringify( u ) );
                list.push( u );
            }

            data.userlist = list;
            data.userlist_empty = false;

            update();
        }
    } );
}

function checkApiAndInitApp() {
    $.ajax( url + "/me" ).done(( c: string ) => {
        initApp();
        initContacts();
        update();
    } ).fail(( jqXHR: any, error: string ) => {
        console.log( "ERROR " + error )
        setTimeout( checkApiAndInitApp, 1000 );
    } );
}

setTimeout( checkApiAndInitApp, 2000 );

