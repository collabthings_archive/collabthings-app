#!/bin/bash

echo COLLABTHINGS-APP BUILD

if [ ! -d "./node_modules" ]; then
	pnpm install
fi


if [ ! -L node_modules/collabthings-api ]; then 
	echo pnpm install collabthings-api
	pnpm install ../collabthings-api
fi

if [ ! -L node_modules/collabthings-app-docs ]; then 
	echo pnpm install collabthings-app-docs
	pnpm install ../collabthings-app-docs
fi

if tsc; then 
	echo tsc success
	browserify client/client.ts -p [ tsify --noImplicitAny ] > assets/bundle.js
	sass src/css/main.scss assets/css/main.css
	echo build success
else
	exit 1;
fi

